package tech.tioft.givestone;

import org.bukkit.plugin.java.JavaPlugin;
import tech.tioft.givestone.die.Die;
import tech.tioft.givestone.givestones.GiveStones;
import tech.tioft.givestone.immortal.ImmortalHandler;
import tech.tioft.givestone.immortal.MakeImmortal;
import tech.tioft.givestone.setspawn.SetSpawn;
import tech.tioft.givestone.setspawn.SpawnListener;

import java.util.Objects;

public class GiveStone extends JavaPlugin {
    @Override
    public void onEnable() {
        Objects.requireNonNull(this.getCommand("givestones")).setExecutor(new GiveStones());
        Objects.requireNonNull(this.getCommand("setborn")).setExecutor(new SetSpawn());
        Objects.requireNonNull(this.getCommand("die")).setExecutor(new Die());
        getServer().getPluginManager().registerEvents(new SpawnListener(), this);
        Objects.requireNonNull(this.getCommand("makeimmortal")).setExecutor(new MakeImmortal());
        getServer().getPluginManager().registerEvents(new ImmortalHandler(), this);
        System.out.println("GiveStone Enabled");
    }
    @Override
    public void onDisable(){

    }
}
