package tech.tioft.givestone.setspawn;

import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;

class SpawnPoints {

    static Map<Integer, Location> sps = new HashMap<>();

    static void setSpawnPoint(Player player, Location location) {
        sps.put(player.getEntityId(), location);
    }

    static void clearSpawnPoint(Player player) {
        sps.remove(player.getEntityId());
    }

    static Location getSpawnPoint(Player player) {
        int id = player.getEntityId();
        if (sps.containsKey(id)) {
            return sps.get(id);
        }
        return null;
    }

}
