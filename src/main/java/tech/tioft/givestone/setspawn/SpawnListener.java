package tech.tioft.givestone.setspawn;

import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerRespawnEvent;

public class SpawnListener implements Listener {
    @EventHandler
    public void onPlayerRespawn(PlayerRespawnEvent pre) {
        Location location = SpawnPoints.getSpawnPoint(pre.getPlayer());
        if (location != null) {
            pre.setRespawnLocation(location);
        }
    }
}
