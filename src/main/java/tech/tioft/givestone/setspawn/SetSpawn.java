package tech.tioft.givestone.setspawn;

import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;


public class SetSpawn implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player)sender;
            Location location = player.getLocation();
            SpawnPoints.setSpawnPoint(player, location);
            sender.sendMessage("Spawn point set at:\n" + location.toString());
        }

        return true;
    }
}

