package tech.tioft.givestone.immortal;
import org.bukkit.entity.Player;

import java.util.ArrayList;

class Immortals {

    private static ArrayList<Integer> players = new ArrayList<>();

    static boolean contains(Player player) {
        return players.contains(player.getEntityId());
    }

    static void add(Player player) {
        players.add(player.getEntityId());
    }

    static void remove(Player player) {
        if (players.contains(player.getEntityId())) {
            players.remove(player.getEntityId());
        }
    }

}
