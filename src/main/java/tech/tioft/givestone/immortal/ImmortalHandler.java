package tech.tioft.givestone.immortal;

import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

public class ImmortalHandler implements Listener {
    @EventHandler
    public void onDamage(EntityDamageEvent evt) {
        Entity entity = evt.getEntity();
        if (entity instanceof Player) {
            Player player = (Player)entity;
            if (Immortals.contains(player)) {
                evt.setCancelled(true);
            }
        }
    }
}
