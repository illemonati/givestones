package tech.tioft.givestone.immortal;


import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

public class MakeImmortal implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Player playerToImmortal = null;
        if (sender instanceof ConsoleCommandSender) {
            if (args.length != 1) {
                sender.sendMessage("You must provide a single argument <player>");
                return false;
            } else {
                playerToImmortal = Bukkit.getPlayer(args[0]);
            }
        } else if (sender instanceof Player) {
            Player senderPlayer = (Player)sender;
            if (!senderPlayer.isOp()) {
                senderPlayer.sendMessage("You must be an op to use this command");
                return false;
            }
            if (args.length == 0) {
                playerToImmortal = senderPlayer;
            } else if (args.length == 1) {
                playerToImmortal = Bukkit.getPlayer(args[0]);
            } else {
                sender.sendMessage("You must provide a single argument <player>");
                return false;
            }
        }

        if (playerToImmortal == null) {
            sender.sendMessage("That didn't work");
            return false;
        }

        Immortals.add(playerToImmortal);
        Bukkit.broadcastMessage(String.format("%s has been made immortal", playerToImmortal.toString()));

        return true;
    }
}

